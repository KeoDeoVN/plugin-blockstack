package mk.plugin.blockstack.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.wasteofplastic.askyblock.ASkyBlockAPI;
import com.wasteofplastic.askyblock.Island;

import mk.plugin.blockstack.history.HistoryGUI;
import mk.plugin.blockstack.history.IslandGUI;

public class AdminCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		
		try {
			
			if (args[0].equalsIgnoreCase("history")) {
				Player viewer = (Player) sender;
				HistoryGUI.open(args[1], viewer);
				sender.sendMessage("§aOpened history gui of " + args[1]);
			}
			
			else if (args[0].equalsIgnoreCase("islandhistory")) {
				Player player = Bukkit.getPlayer(args[1]);
				Island is = ASkyBlockAPI.getInstance().getIslandOwnedBy(player.getUniqueId());
				if (is == null) {
					sender.sendMessage("§c" + player.getName() + " has no island");
					return false;
				}
				IslandGUI.open(is, (Player) sender);
				sender.sendMessage("§aOpened island gui of " + args[1]);
			}
			
		}
		catch (ArrayIndexOutOfBoundsException e) {
			sendHelp(sender);
		}
		
		return false;
	}
	
	public void sendHelp(CommandSender sender) {
		sender.sendMessage("§2§lBlockStack admin commands");
		sender.sendMessage("§a/bsa history <player>");
		sender.sendMessage("§a/bsa islandhistory <player>");
	}

}
