package mk.plugin.blockstack.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.wasteofplastic.askyblock.ASkyBlockAPI;
import com.wasteofplastic.askyblock.Island;

import mk.plugin.blockstack.history.IslandGUI;

public class PlayerCommand implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		
		try {
			
			if (args[0].equalsIgnoreCase("history") || args[0].equalsIgnoreCase("log")) {
				Player player = (Player) sender;
				Island is = ASkyBlockAPI.getInstance().getIslandOwnedBy(player.getUniqueId());
				if (is == null) {
					sender.sendMessage("§cBạn không có island");
					return false;
				}
				IslandGUI.open(is, player);
			}
			
		}
		catch (ArrayIndexOutOfBoundsException e) {
			sendHelp(sender);
		}
		
		return false;
	}
	
	public void sendHelp(CommandSender sender) {
		sender.sendMessage("");
		sender.sendMessage("§a/blockstack log: §7Xem lịch sửa đặt/lấy tại block tại island");
		sender.sendMessage("§a/blockstack history: §7Xem lịch sửa đặt/lấy tại block tại island");
		sender.sendMessage("");
	}
	
}
