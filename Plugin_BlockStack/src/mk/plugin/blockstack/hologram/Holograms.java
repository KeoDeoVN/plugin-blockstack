package mk.plugin.blockstack.hologram;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

import mk.plugin.blockstack.config.Configs;
import mk.plugin.blockstack.data.StackData;
import net.minecraft.server.v1_12_R1.EntityArmorStand;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_12_R1.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_12_R1.WorldServer;

public class Holograms {
	
	private static Map<Block, EntityArmorStand> livingHolograms = Maps.newHashMap();
	
	public static void sendPacket(Player player, StackData data, boolean overwrite) {
		if (!overwrite) if (livingHolograms.containsKey(data.getLocation().getBlock())) return;
		sendPacket(player, data);
	}
	
	public static void sendPacket(Player player, StackData data) {
		String name = Configs.TRANS.getOrDefault(data.getLocation().getBlock().getType().name(), data.getLocation().getBlock().getType().name());
		String holo = Configs.HOLOGRAM.replace("%name%", name).replace("%amount%", data.getAmount() + "");
		
		EntityArmorStand stand = null;
		if (livingHolograms.containsKey(data.getLocation().getBlock())) {
			stand = livingHolograms.get(data.getLocation().getBlock());
		} else {
			WorldServer world = ((CraftWorld) data.getLocation().getWorld()).getHandle();
			stand = new EntityArmorStand(world);
	        
	        stand.setLocation(data.getLocation().getX() + 0.5, data.getLocation().getY() - 0.6, data.getLocation().getZ() + 0.5, 0, 0);
	        stand.setCustomNameVisible(true);
	        stand.setNoGravity(true);
	        stand.setInvisible(true);
	        
	        livingHolograms.put(data.getLocation().getBlock(), stand);
		}
		
        stand.setCustomName(holo);
        
        PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(stand);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
	}
	
	public static void remove(Block block) {
		if (!livingHolograms.containsKey(block)) return;
		EntityArmorStand stand = livingHolograms.get(block);
		Bukkit.getOnlinePlayers().forEach(player -> {
			PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(stand.getId());
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		});
		livingHolograms.remove(block);
	}
	
}
