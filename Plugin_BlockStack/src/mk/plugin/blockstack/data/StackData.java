package mk.plugin.blockstack.data;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

import com.google.common.collect.Lists;

public class StackData {
	
	private int amount;
	private TempLocation location;
	
	public StackData(int amount, TempLocation location) {
		this.amount = amount;
		this.location = location;
	}
	
	public StackData(int amount, Location location) {
		this.amount = amount;
		this.location = new TempLocation(location.getX(), location.getY(), location.getZ(), location.getWorld().getName());
	}
	
	public int getAmount() {
		return this.amount;
	}
	
	public TempLocation getTempLocation() {
		return this.location;
	}
	
	public Location getLocation() {
		return new Location(Bukkit.getWorld(this.location.getWorld()), this.location.getX(), this.location.getY(), this.location.getZ());
	}
	
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	// Static
	
	public static List<StackData> data = Lists.newArrayList();
	
	public static void reload(FileConfiguration config) {
		data.clear();
		config.getConfigurationSection("block-stack").getKeys(false).forEach(id -> {
			int amount = config.getInt("block-stack." + id + ".amount");
			double x = config.getDouble("block-stack." + id + ".location.x");
			double y = config.getDouble("block-stack." + id + ".location.y");
			double z = config.getDouble("block-stack." + id + ".location.z");
			String world = config.getString("block-stack." + id + ".location.world");
			TempLocation location = new TempLocation(x, y, z, world);
			data.add(new StackData(amount, location));
		});
	}
	
	public static void setAll(FileConfiguration config) {
		int c = -1;
		for (StackData data : StackData.data) {
			c++;
			String path = "block-stack." + c;
			config.set(path + ".amount", data.getAmount());
			config.set(path + ".location.x", data.getLocation().getX());
			config.set(path + ".location.y", data.getLocation().getY());
			config.set(path + ".location.z", data.getLocation().getZ());
			config.set(path + ".location.world", data.getLocation().getWorld().getName());
		}
	}
	
	public static void checkAll() {
		for (StackData data : Lists.newArrayList(StackData.data)) {
			if (data.getLocation().getBlock().getType() == Material.AIR) StackData.data.remove(data); 
		}
	}
	
}
