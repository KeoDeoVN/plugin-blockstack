package mk.plugin.blockstack.data;

import org.bukkit.Location;

public class TempLocation {
	
	private double x;
	private double y;
	private double z;
	private String world;
	
	public TempLocation(Location location) {
		this.x = location.getX();
		this.y = location.getY();
		this.z = location.getZ();
		this.world = location.getWorld().getName();
	}
	
	public TempLocation(double x, double y, double z, String world) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.world = world;
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	public double getZ() {
		return this.z;
	}
	
	public String getWorld() {
		return this.world;
	}
	
	@Override
	public String toString() {
		return this.x + ";" + this.y + ";" + this.z + ";" + this.world;
	}
	
	public static TempLocation parse(String s) {
		String[] p = s.split(";");
		return new TempLocation(Double.valueOf(p[0]), Double.valueOf(p[1]), Double.valueOf(p[2]), p[3]);
	}
	
}
