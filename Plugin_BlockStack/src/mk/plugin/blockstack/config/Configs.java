package mk.plugin.blockstack.config;

import java.util.List;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.blockstack.take.TakeType;

public class Configs {
	
	public static TakeType LEFT_TAKE;
	public static TakeType SHIFT_LEFT_TAKE;
	public static List<String> ALLOW_BLOCKS = Lists.newArrayList();
	public static List<String> ALLOW_WORLDS = Lists.newArrayList();
	public static Map<String, String> TRANS = Maps.newHashMap();
	public static String HOLOGRAM;
	public static Map<String, Double> BLOCK_LEVELS = Maps.newHashMap();
	
	public static void reload(FileConfiguration config) {
		LEFT_TAKE = TakeType.valueOf(config.getString("left").toUpperCase());
		SHIFT_LEFT_TAKE = TakeType.valueOf(config.getString("shift-left").toUpperCase());
		ALLOW_BLOCKS = config.getStringList("allow-blocks");
		ALLOW_WORLDS = config.getStringList("allow-worlds");
		TRANS.clear();
		config.getConfigurationSection("trans").getKeys(false).forEach(m -> {
			TRANS.put(m, config.getString("trans." + m));
		});
		HOLOGRAM = config.getString("hologram").replace("&", "§");
		BLOCK_LEVELS.clear();
		config.getConfigurationSection("block-level").getKeys(false).forEach(m -> {
			BLOCK_LEVELS.put(m, config.getDouble("block-level." + m));
		});
	}
	
}
