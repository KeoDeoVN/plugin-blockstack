package mk.plugin.blockstack.main;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import mk.plugin.blockstack.command.AdminCommand;
import mk.plugin.blockstack.command.PlayerCommand;
import mk.plugin.blockstack.config.Configs;
import mk.plugin.blockstack.data.StackData;
import mk.plugin.blockstack.history.StackHistory;
import mk.plugin.blockstack.listener.BlockListener;
import mk.plugin.blockstack.listener.HologramListener;
import mk.plugin.blockstack.listener.IslandListener;
import mk.plugin.blockstack.listener.PlayerListener;

public class MainBlockStack extends JavaPlugin {
		
	private FileConfiguration config;
	private FileConfiguration dataConfig;
	
	@Override
	public void onEnable() {
		this.saveDefaultConfig();
		this.reloadConfig();
		
		Bukkit.getPluginManager().registerEvents(new BlockListener(), this);
		Bukkit.getPluginManager().registerEvents(new HologramListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
		Bukkit.getPluginManager().registerEvents(new IslandListener(), this);
		
		this.getCommand("blockstackadmin").setExecutor(new AdminCommand());
		this.getCommand("blockstack").setExecutor(new PlayerCommand());
	}
	
	public void reloadConfig() {
		this.config = YamlConfiguration.loadConfiguration(new File(this.getDataFolder(), "config.yml"));
		Configs.reload(this.getConfig());
		
		File dataFile = new File(this.getDataFolder(), "data.yml");
		if (!dataFile.exists()) {
			try {
				FileUtils.copyInputStreamToFile(this.getResource("data.yml"), dataFile);
				dataFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		this.dataConfig = YamlConfiguration.loadConfiguration(dataFile);
		StackData.reload(dataConfig);
		StackHistory.reload(this);
	}
	
	public void saveConfig() {
		try {
			StackData.setAll(this.dataConfig);
			this.dataConfig.save(new File(this.getDataFolder(), "data.yml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public FileConfiguration getConfig() {
		return this.config;
	}
	
	public static MainBlockStack get() {
		return MainBlockStack.getPlugin(MainBlockStack.class);
	}
	
}
