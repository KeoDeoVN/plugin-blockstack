package mk.plugin.blockstack.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import mk.plugin.blockstack.data.StackData;
import mk.plugin.blockstack.history.HistoryChecker;
import mk.plugin.blockstack.history.HistoryGUI;
import mk.plugin.blockstack.history.IslandGUI;

public class PlayerListener implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		StackData.checkAll();
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		HistoryChecker.lastSave(e.getPlayer());
	}
	
	@EventHandler
	public void onGUIClick(InventoryClickEvent e) {
		HistoryGUI.eventClick(e);
		IslandGUI.eventClick(e);
	}
	
}
