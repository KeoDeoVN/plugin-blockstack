package mk.plugin.blockstack.listener;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.wasteofplastic.askyblock.ASkyBlockAPI;

import mk.plugin.blockstack.config.Configs;
import mk.plugin.blockstack.data.StackData;
import mk.plugin.blockstack.data.TempLocation;
import mk.plugin.blockstack.history.HistoryChecker;
import mk.plugin.blockstack.history.StackAction;
import mk.plugin.blockstack.history.StackHistory;
import mk.plugin.blockstack.hologram.Holograms;
import mk.plugin.blockstack.main.MainBlockStack;
import mk.plugin.blockstack.take.TakeType;

public class BlockListener implements Listener {
	
	@EventHandler
	public void onBlockMove(BlockPistonExtendEvent e) {
		e.getBlocks().stream().map(b -> b.getType().name()).collect(Collectors.toList()).forEach(t -> {
			if (Configs.ALLOW_BLOCKS.contains(t)) {
				e.setCancelled(true);
				return;
			}
		});
	}
	
	@EventHandler
	public void onTake(PlayerInteractEvent e) {
		if (!e.getAction().name().contains("BLOCK")) return;
		if (e.getAction() == Action.LEFT_CLICK_BLOCK) {
			Player player = e.getPlayer();
			if (!isOnOwnIsland(player)) return;
			
			Block block = e.getClickedBlock();
			StackData data = from(block.getLocation());
			if (data == null) return;
			
			int amount = 1;
			
			// Check amount
			if (player.isSneaking()) {
				if (Configs.SHIFT_LEFT_TAKE == TakeType.TAKE_64) amount = Math.min(64, data.getAmount());
				else if (Configs.SHIFT_LEFT_TAKE == TakeType.TAKE_ALL) amount = data.getAmount();
			}
			else {
				if (Configs.LEFT_TAKE == TakeType.TAKE_64) amount = Math.min(64, data.getAmount());
				else if (Configs.LEFT_TAKE == TakeType.TAKE_ALL) amount = data.getAmount();
			}
			
			// Give
			player.getInventory().addItem(new ItemStack(block.getType(), amount));
			player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
			
			// Remove
			data.setAmount(data.getAmount() - amount);
			if (data.getAmount() <= 0) {
				block.setType(Material.AIR);
				Holograms.remove(block);
				StackData.data.remove(data);
				MainBlockStack.get().saveConfig();
			}
			else {
				Holograms.sendPacket(player, data, true);
			}
			
			// Write history
			int finalamount = amount;
			Bukkit.getScheduler().runTaskAsynchronously(MainBlockStack.get(), () -> {
				StackHistory sh = new StackHistory(player.getName(), new Date(), new TempLocation(block.getLocation()), StackAction.TAKE, finalamount);
				HistoryChecker.addHistory(player, sh);
			});
		}
	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		Block block = e.getBlock();
		if (from(block.getLocation()) != null) e.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlace(BlockPlaceEvent e) {
		if (e.isCancelled()) return;
		Block block = e.getBlock();
		Player player = e.getPlayer();
		
		// Check allow
		if (!Configs.ALLOW_WORLDS.contains(block.getWorld().getName())) return;
		if (!Configs.ALLOW_BLOCKS.contains(block.getType().name())) return;
		
		// Check block
		Location near = near(block);
		if (near == null) return;
		
		StackData data = null;
		if (from(near) != null) {
			data = from(near);
		} else {
			data = new StackData(1, near);
			StackData.data.add(data);
		}
		
		int amountBonus = 1;
		// Check shilf
		if (player.isSneaking()) {
			ItemStack is = player.getInventory().getItemInMainHand();
			amountBonus = is.getAmount();
			Bukkit.getScheduler().runTaskLater(MainBlockStack.get(), () -> {
				is.setAmount(0);
				player.updateInventory();
			}, 2);
		}
		data.setAmount(data.getAmount() + amountBonus);
		Holograms.sendPacket(player, data, true);
		
		// Set
		Bukkit.getScheduler().runTask(MainBlockStack.get(), () -> {
			block.setType(Material.AIR);
			MainBlockStack.get().saveConfig();
		});
		
		// Write history
		int finalamount = amountBonus;
		Bukkit.getScheduler().runTaskAsynchronously(MainBlockStack.get(), () -> {
			StackHistory sh = new StackHistory(player.getName(), new Date(), new TempLocation(block.getLocation()), StackAction.PLACE, finalamount);
			HistoryChecker.addHistory(player, sh);
		});
	}
	
	private Location near(Block block) {
		Location current = block.getLocation();
		
		List<Location> list = Lists.newArrayList();
		
		list.add(new Location(current.getWorld(), current.getX() + 1, current.getY(), current.getZ()));
		list.add(new Location(current.getWorld(), current.getX() - 1, current.getY(), current.getZ()));
		list.add(new Location(current.getWorld(), current.getX(), current.getY() + 1, current.getZ()));
		list.add(new Location(current.getWorld(), current.getX(), current.getY() - 1, current.getZ()));
		list.add(new Location(current.getWorld(), current.getX(), current.getY(), current.getZ() + 1));
		list.add(new Location(current.getWorld(), current.getX(), current.getY(), current.getZ() - 1));
		
		for (Location l : list) {
			if (l.getBlock().getType() == block.getType()) return l;
		}
		
		return null;
	}
	
	private StackData from(Location loc) {
		for (StackData data : StackData.data) {
			if (data.getLocation().getWorld() != loc.getWorld()) continue;
			if (data.getLocation().getBlock().getLocation().distance(loc.getBlock().getLocation()) < 0.5) return data;
		}
		return null;
	}
	
	private boolean isOnOwnIsland(Player player) {
		if (ASkyBlockAPI.getInstance().getIslandAt(player.getLocation()) == null) return false;
		return ASkyBlockAPI.getInstance().getIslandAt(player.getLocation()).getMembers().contains(player.getUniqueId());
	}
	
}
