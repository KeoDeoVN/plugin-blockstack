package mk.plugin.blockstack.listener;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.wasteofplastic.askyblock.ASkyBlockAPI;
import com.wasteofplastic.askyblock.Island;
import com.wasteofplastic.askyblock.events.IslandPreLevelEvent;

import mk.plugin.blockstack.config.Configs;
import mk.plugin.blockstack.data.StackData;

public class IslandListener implements Listener {

	@EventHandler
	public void onLevelCalculate(IslandPreLevelEvent e) {
		Island is = e.getIsland();

		long alevel = e.getLongLevel();
		// Check block on player's island
		for (StackData data : StackData.data) {
			Island check = ASkyBlockAPI.getInstance().getIslandAt(data.getLocation());
			if (check == null)
				continue;
			if (is.getOwner().equals(check.getOwner())) {
				Block block = data.getLocation().getBlock();
				alevel += (data.getAmount() - 1) * Configs.BLOCK_LEVELS.getOrDefault(block.getType().name(), 0d);
			}
		}
		
		e.setLongLevel(alevel);
	}

}
