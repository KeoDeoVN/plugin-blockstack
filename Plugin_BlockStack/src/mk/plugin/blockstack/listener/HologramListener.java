package mk.plugin.blockstack.listener;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.metadata.FixedMetadataValue;

import mk.plugin.blockstack.data.StackData;
import mk.plugin.blockstack.hologram.Holograms;
import mk.plugin.blockstack.main.MainBlockStack;

public class HologramListener implements Listener {
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player player = e.getPlayer();
		StackData.data.forEach(data -> {
			Location fl = data.getLocation();
			String id = fl.getBlock().getLocation().toString();
			if (player.getWorld() == fl.getWorld()) {
				if (fl.distance(player.getLocation()) < 25) {
					if (player.hasMetadata("flower." + id)) return;
					player.setMetadata("flower." + id, new FixedMetadataValue(MainBlockStack.get(), ""));
					Holograms.sendPacket(player, data);
					return;
				}
			}
			else if (player.hasMetadata("flower." + id)) player.removeMetadata("flower." + id, MainBlockStack.get());
		});
	}
	
	@EventHandler
	public void onTele(PlayerTeleportEvent e) {
		Player player = e.getPlayer();
		StackData.data.forEach(data -> {
			Location fl = data.getLocation();
			String id = fl.getBlock().getLocation().toString();
			if (e.getTo().getWorld() != fl.getWorld()) {
				if (player.hasMetadata("flower." + id)) player.removeMetadata("flower." + id, MainBlockStack.get());
			}
		});
	}
}
