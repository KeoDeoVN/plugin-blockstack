package mk.plugin.blockstack.take;

public enum TakeType {

	TAKE_1,
	TAKE_64,
	TAKE_ALL;
	
}
