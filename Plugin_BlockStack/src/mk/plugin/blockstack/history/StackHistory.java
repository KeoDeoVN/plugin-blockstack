package mk.plugin.blockstack.history;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.bukkit.plugin.Plugin;

import com.google.common.collect.Lists;

import mk.plugin.blockstack.data.TempLocation;

public class StackHistory {

	private String player;
	private Date date;
	private TempLocation location;
	private StackAction action;
	private int amount;

	public StackHistory(String player, Date date, TempLocation location, StackAction action, int amount) {
		this.player = player;
		this.date = date;
		this.location = location;
		this.action = action;
		this.amount = amount;
	}

	public String getPlayer() {
		return this.player;
	}

	public Date getDate() {
		return this.date;
	}

	public TempLocation getLocation() {
		return this.location;
	}

	public StackAction getAction() {
		return this.action;
	}

	public int getAmount() {
		return this.amount;
	}

	public void addAmount(int add) {
		this.amount += add;
	}
	
	public String dateToString() {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return df.format(this.getDate()); 
	}
	
	public static StackHistory parse(String s) {
		String[] parts = s.split("\\s{1}\\|\\s{1}");
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = null;
		try {
			date = df.parse(parts[0]);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		String player = parts[1];
		TempLocation location = TempLocation.parse(parts[2]);
		StackAction action = StackAction.valueOf(parts[3]);
		int amount = Integer.parseInt(parts[4]);

		return new StackHistory(player, date, location, action, amount);
	}

	@Override
	public String toString() {
		return dateToString() + " | " + this.getPlayer() + " | " + this.getLocation().toString() + " | " + this.action.name()
				+ " | " + this.amount;
	}

	public static List<StackHistory> history = Lists.newArrayList();

	public static void reload(Plugin plugin) {
		File file = new File(plugin.getDataFolder(), "history.txt");
		if (!file.exists())
			return;

		List<StackHistory> list = Lists.newArrayList();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));

		String line = null;

		try {
			while ((line = br.readLine()) != null) {
				StackHistory sh = StackHistory.parse(line);
				list.add(sh);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		history = list;
	}

}
