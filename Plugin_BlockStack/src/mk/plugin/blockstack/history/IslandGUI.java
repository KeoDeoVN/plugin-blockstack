package mk.plugin.blockstack.history;

import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import com.google.common.collect.Lists;
import com.wasteofplastic.askyblock.Island;

import mk.plugin.blockstack.main.MainBlockStack;

public class IslandGUI {
	
	public static void open(Island is, Player viewer) {
		Inventory inv = Bukkit.createInventory(new IGHolder(is), 18, "§8§lLỊCH SỬ");
		viewer.openInventory(inv);
		viewer.playSound(viewer.getLocation(), Sound.BLOCK_CHEST_OPEN, 1, 1);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainBlockStack.get(), () -> {
			for (int i = 0 ; i < inv.getSize() ; i ++) inv.setItem(i, getBlank());
			int count = -1;
			for (UUID uuid : is.getMembers()) {
				count++;
				inv.setItem(count, getHead(uuid));
			}
		});
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getInventory().getHolder() instanceof IGHolder == false) return;
		e.setCancelled(true);
		if (e.getClickedInventory() != e.getWhoClicked().getOpenInventory().getTopInventory()) return;
		
		int slot = e.getSlot();
		IGHolder holder = (IGHolder) e.getInventory().getHolder();
		if (slot >= holder.getIsland().getMembers().size()) return;
		Player viewer = (Player) e.getWhoClicked();
		UUID member = holder.getIsland().getMembers().get(slot);
		String name = Bukkit.getOfflinePlayer(member).getName();
		HistoryGUI.open(name, viewer);
	}
	
	public static ItemStack getBlank() {
		ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName("§e ");
		is.setItemMeta(meta);
		return is;
	}
	
	public static ItemStack getHead(UUID uuid) {
		ItemStack item = new ItemStack(Material.SKULL_ITEM, 1 , (short) 3);
		SkullMeta meta = (SkullMeta) item.getItemMeta();
		OfflinePlayer op = Bukkit.getOfflinePlayer(uuid);
		meta.setOwningPlayer(op);
		meta.setDisplayName("§c§l" + op.getName());
		List<String> lore = Lists.newArrayList();
		lore.add("§7Click để xem lịch sử của người chơi này");
		meta.setLore(lore);
		item.setItemMeta(meta);
		
		return item;
	}
	
}

class IGHolder implements InventoryHolder {
	
	private Island is;
	
	public IGHolder(Island is) {
		this.is = is;
	}
	
	public Island getIsland() {
		return this.is;
	}
	
	@Override
	public Inventory getInventory() {
		return null;
	}
	
}
 