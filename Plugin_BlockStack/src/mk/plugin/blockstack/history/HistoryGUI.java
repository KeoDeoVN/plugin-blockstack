package mk.plugin.blockstack.history;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import mk.plugin.blockstack.main.MainBlockStack;

public class HistoryGUI {
	
	public static void open(String player, Player viewer) {
		Inventory inv = Bukkit.createInventory(new HGHolder(player), 36, "§8§lLỊCH SỬ");
		viewer.openInventory(inv);
		viewer.playSound(viewer.getLocation(), Sound.BLOCK_CHEST_OPEN, 1, 1);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainBlockStack.get(), () -> {
			for (int i = 0 ; i < inv.getSize() ; i ++) inv.setItem(i, getBlank());
			if (Bukkit.getPlayer(player) != null) HistoryChecker.lastSave(Bukkit.getPlayer(player));
			List<StackHistory> list = getOf(player);
			int slot = -1;
			for (int i = Math.max(0, list.size() - inv.getSize()) ; i < list.size() ; i++) {
				slot++;
				inv.setItem(slot, getIcon(list.get(i)));
			}
		});
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getInventory().getHolder() instanceof HGHolder == false) return;
		e.setCancelled(true);
	}
	
	public static List<StackHistory> getOf(String player) {
		List<StackHistory> list = Lists.newArrayList();
		StackHistory.history.forEach(sh -> {
			if (sh.getPlayer().equalsIgnoreCase(player)) list.add(sh);
		});
		return list;
	}
	
	public static ItemStack getIcon(StackHistory sh) {
		Material m = sh.getAction() == StackAction.PLACE ? Material.EMPTY_MAP : Material.PAPER;
		ItemStack is = new ItemStack(m, 1);
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName("§6§l" + sh.dateToString());
		List<String> lore = Lists.newArrayList();
		lore.add("§eHành động: §f" + sh.getAction().getName());
		lore.add("§eSố lượng: §f" + sh.getAmount());
		lore.add("§eVị trí: §f" + sh.getLocation().toString());
		meta.setLore(lore);
		is.setItemMeta(meta);
		return is;
	}
	
	public static ItemStack getBlank() {
		ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName("§e ");
		is.setItemMeta(meta);
		return is;
	}
	
}

class HGHolder implements InventoryHolder {

	private String player;
	
	public HGHolder(String player) {
		this.player = player;
	}
	
	public String getPlayer() {
		return this.player;
	}
	
	@Override
	public Inventory getInventory() {
		return null;
	}
	
	
	
}