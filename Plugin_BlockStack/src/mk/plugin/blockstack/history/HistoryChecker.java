package mk.plugin.blockstack.history;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.google.common.collect.Maps;

import mk.plugin.blockstack.main.MainBlockStack;

public class HistoryChecker {
	
	private static final int COOLDOWN_MILIS = 5000;
	
	private static Map<String, Long> cooldown = Maps.newHashMap();
	private static Map<String, StackHistory> playerHistory = Maps.newHashMap();
	
	private static void setCooldown(Player player) {
		cooldown.put(player.getName(), System.currentTimeMillis() + COOLDOWN_MILIS);
	}
	
	public static void addHistory(Player player, StackHistory history) {
		if (playerHistory.containsKey(player.getName())) {
			StackHistory sh = playerHistory.get(player.getName());
			if (sh.getAction() == history.getAction() && cooldown.get(player.getName()) > System.currentTimeMillis()) {
				sh.addAmount(history.getAmount());
				return;
			}
			HistoryChecker.write(MainBlockStack.get(), sh);
		}
		playerHistory.put(player.getName(), history);
		setCooldown(player);
	}
	
	public static void lastSave(Player player) {
		if (!playerHistory.containsKey(player.getName())) return;
		StackHistory sh = playerHistory.get(player.getName());
		HistoryChecker.write(MainBlockStack.get(), sh);
		playerHistory.remove(player.getName());
		
	}
	
	public static void write(Plugin plugin, StackHistory history) {
		// Check file
		File file = new File(plugin.getDataFolder(), "history.txt");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		// Write
		String line = history.toString();
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));	
		    out.println(line);
		    out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}
