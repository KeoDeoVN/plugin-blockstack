package mk.plugin.blockstack.history;

public enum StackAction {
	
	TAKE("Lấy"),
	PLACE("Đặt");
	
	private String name;
	
	private StackAction(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
}
